//
//  UIView.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/28/21.
//

import UIKit

extension UIView {
    /**
     Loads view from nib, considering that nib name is same as class name.
     */
    func loadFromNib() {
        let nibName = String(describing: type(of: self))
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        nib.instantiate(withOwner: self)
    }
}
