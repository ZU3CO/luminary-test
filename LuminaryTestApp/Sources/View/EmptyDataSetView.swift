//
//  EmptyDataSetView.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/28/21.
//

import UIKit
import SDWebImage

class EmptyDataSetView: UIView {
    
    /// Any subviews should be added to contentView
    @IBOutlet weak private(set) var contentView: UIView!
    
    // MARK: - Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        loadFromNib()
        
        contentView.frame = bounds
        
        addSubview(contentView)
    }
}
