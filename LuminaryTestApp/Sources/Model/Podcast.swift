//
//  Podcast.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/27/21.
//

import Foundation

struct Images: Decodable, Hashable {
    let `default`: URL?
    let featured: URL?
    let thumbnail: URL?
    let wide: URL?
}

struct Podcast: Decodable, Hashable {
    let id: String
    let title: String
    let images: Images
    let isExclusive: Bool
    let publisherName: String
    let publisherId: String
    let mediaType: String
    let description: String
    let categoryId: String
    let categoryName: String
    let hasFreeEpisodes: Bool
    let playSequence: String
}
