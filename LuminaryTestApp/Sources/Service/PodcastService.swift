//
//  PodcastsService.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/27/21.
//

import Foundation
import Combine


enum PodcastServiceError: Error {
    case url(URLError)
    case urlRequest
    case decode
}

enum SearchOption {
    case none
    case generic
    case title
    case category
    
    var queryName: String {
        switch self {
        case .none:
            return ""
        case .generic:
            return "search"
        case .title:
            return "title"
        case .category:
            return "categoryName"
        }
    }
}

protocol PodcastServiceProcol {
    func getAllPodcasts(page: Int?) -> AnyPublisher<[Podcast], Error>
    func getPodcasts(searchTerm: String?, option: SearchOption) -> AnyPublisher<[Podcast], Error>
}

final class PodcastService: PodcastServiceProcol {
    
    static let pageSize = 10
    
    func getAllPodcasts(page: Int?) -> AnyPublisher<[Podcast], Error> {
        getPodcasts(page: page)
    }
    
    func getPodcasts(searchTerm: String?, option: SearchOption) -> AnyPublisher<[Podcast], Error> {
        getPodcasts(searchTerm: searchTerm, option: option, page: nil)
    }
    
    private func getPodcasts(searchTerm: String? = nil, option: SearchOption = .none, page: Int? = nil) -> AnyPublisher<[Podcast], Error> {
        var dataTask: URLSessionDataTask?
        let onSubscription: (Subscription) -> () = { _ in dataTask?.resume() }
        let onCancel: () -> () = { dataTask?.cancel() }

        return Future<[Podcast], Error> { [weak self] promise in
            guard let urlRequest = self?.getUrlRequest(searchTerm: searchTerm, option: option, page: page) else {
                promise(.failure(PodcastServiceError.urlRequest))
                return
            }
            
            dataTask = URLSession.shared.dataTask(with: urlRequest) { data, _, error in
                guard let data = data else {
                    if let error = error {
                        promise(.failure(error))
                    }
                    return
                }
                do {
                    let podcasts = try JSONDecoder().decode([Podcast].self, from: data)
                    
                    promise(.success(podcasts))
                } catch {
                    promise(.failure(PodcastServiceError.decode))
                }
            }
        }
        .handleEvents(receiveSubscription: onSubscription, receiveCancel: onCancel)
        .receive(on: DispatchQueue.main)
        .eraseToAnyPublisher()
    }
    
    private func getUrlRequest(searchTerm: String? = nil, option: SearchOption = .none, page: Int? = nil) -> URLRequest? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "601f1754b5a0e9001706a292.mockapi.io"
        components.path = "/podcasts"
        if let searchTerm = searchTerm, !searchTerm.isEmpty {
            components.queryItems = [
                URLQueryItem(name: option.queryName, value: searchTerm)
            ]
        }
        if let page = page {
            components.queryItems = [
                URLQueryItem(name: "page", value: String(page)),
                URLQueryItem(name: "limit", value: String(PodcastService.pageSize)),
            ]
        }
        
        guard let url = components.url else { return nil }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = 10.0
        urlRequest.httpMethod = "GET"
        
        return urlRequest
    }
}
