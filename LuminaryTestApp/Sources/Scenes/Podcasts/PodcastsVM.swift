//
//  PodcastsVM.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/27/21.
//

import Foundation
import Combine

enum PodcastsVMState {
    case isLoading
    case loaded
    case error(Error)
}

final class PodcastsVM {
    struct PageState {
        var page: Int = 1
        var canLoadNextPage = true
    }
    enum Section { case items }
    
    @Published var searchText: String = ""
    @Published private(set) var items: [Podcast] = []
    @Published private(set) var state: PodcastsVMState = .isLoading
    @Published private(set) var pageState = PageState()
    
    private let service: PodcastServiceProcol
    private var bindings = Set<AnyCancellable>()
    
    init(podcastService: PodcastServiceProcol = PodcastService()) {
        self.service = podcastService
        
        $searchText
            .dropFirst()
            .sink(receiveValue: searchPodcast(with:))
            .store(in: &bindings)
        
        fetchPodcasts()
    }
    
    func searchPodcast(with searchTerm: String? = nil) {
        state = .isLoading
        
        pageState.canLoadNextPage = true
        service
            .getPodcasts(searchTerm: searchTerm, option: .generic)
            .sink(receiveCompletion: onReceive, receiveValue: onSearchReceiveValue)
            .store(in: &bindings)
    }
    
    func fetchPodcasts(with searchTerm: String? = nil) {
        guard pageState.canLoadNextPage else {
            state = .loaded
            return
        }
        state = .isLoading
        
        service
            .getAllPodcasts(page: pageState.page)
            .sink(receiveCompletion: onReceive, receiveValue: onReceiveValue)
            .store(in: &bindings)
    }
    
    func resetLoadedItems() {
        items = []
        
        // Reset paging state
        pageState.page = 1
        pageState.canLoadNextPage = true
    }
    
    private func onReceive(_ completion: Subscribers.Completion<Error>) {
        switch completion {
        case .failure(let error):
            state = .error(error)
            pageState.canLoadNextPage = false
        case .finished:
            state = .loaded
        }
    }
    
    private func onReceiveValue(_ batch: [Podcast]) {
        pageState.page += 1
        pageState.canLoadNextPage = batch.count == PodcastService.pageSize
        
        for item in batch {
            if !items.contains(item) {
                items.append(item)
            } else {
                // If last batch contains old elements, them all elements fetched
                pageState.canLoadNextPage = false
            }
        }
    }
    
    private func onSearchReceiveValue(_ batch: [Podcast]) {
        items = batch
    }
}
