//
//  PodcastsVC.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/27/21.
//

import UIKit
import Combine
import SwifterSwift

class PodcastsVC: UIViewController {
    private typealias DataSource = UITableViewDiffableDataSource<PodcastsVM.Section, Podcast>
    private typealias Snapshot = NSDiffableDataSourceSnapshot<PodcastsVM.Section, Podcast>
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    private var emptyDataSetView: EmptyDataSetView?
    
    private var bindings = Set<AnyCancellable>()
    private var viewModel: PodcastsVM!
    private var dataSource: DataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = PodcastsVM()
        
        setupTableView()
        setupDataSource()
        setupBindings()
    }
    
    func setupTableView() {
        tableView.delegate = self
        
        tableView.register(nibWithCellClass: PodcastTableViewCell.self)
        
        // Setup tableView's refresh control
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(fetchPodcasts), for: .valueChanged)
        refreshControl.beginRefreshing()
        tableView.refreshControl = refreshControl
        
        // Hide separator for empty cells
        tableView.tableFooterView = UIView()
    }
    
    func setupDataSource() {
        dataSource = DataSource(
            tableView: tableView,
            cellProvider: { (tableView, indexPath, podcast) -> UITableViewCell? in
                let cell = tableView.dequeueReusableCell(withClass: PodcastTableViewCell.self, for: indexPath)
                cell.setupSubviews(with: PodcastCellVM(podcast: podcast))
                
                return cell
            })
    }
    
    func setupBindings() {
        searchBar.searchTextField.textPublisher
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .removeDuplicates()
            .assign(to: \.searchText, on: viewModel)
            .store(in: &bindings)
        
        viewModel.$items
            .receive(on: RunLoop.main)
            .sink { [weak self] _ in
                self?.updateSections()
                self?.updateEmptyDataSetView()
            }
            .store(in: &bindings)
        
        let stateValueHandler: (PodcastsVMState) -> () = { [weak self] state in
            switch state {
            case .isLoading:
                self?.startLoading()
            case .loaded:
                self?.stopLoading()
            case .error(let error):
                self?.stopLoading()
                self?.showError(error)
            }
        }
        
        viewModel.$state
            .receive(on: RunLoop.main)
            .sink(receiveValue: stateValueHandler)
            .store(in: &bindings)
    }
    
    @objc func fetchPodcasts() {
        viewModel.resetLoadedItems()
        viewModel.fetchPodcasts()
    }
    
    func startLoading() {
        tableView.refreshControl?.beginRefreshing()
        
        removeEmptyDataSetView()
    }
    
    func stopLoading() {
        tableView.refreshControl?.endRefreshing()
        
        updateEmptyDataSetView()
        hideBottomSpinner()
    }
    
    func updateEmptyDataSetView() {
        if viewModel.items.isEmpty {
            addEmptyDataSetView()
        } else {
            removeEmptyDataSetView()
        }
    }
    
    func addEmptyDataSetView() {
        let view = EmptyDataSetView(frame: tableView.bounds)
        
        if emptyDataSetView?.superview != nil {
            emptyDataSetView?.removeFromSuperview()
        }
        
        tableView.addSubview(view)
        emptyDataSetView = view
    }
    
    func removeEmptyDataSetView() {
        emptyDataSetView?.removeFromSuperview()
    }
    
    func updateSections() {
        var snapshot = Snapshot()
        snapshot.appendSections([.items])
        snapshot.appendItems(viewModel.items)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    func showError(_ error: Error) {
        let alert = UIAlertController(title: error.localizedDescription)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showBottomSpinner() {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.startAnimating()
        tableView.tableFooterView = activityIndicator
    }
    
    func hideBottomSpinner() {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.startAnimating()
        tableView.tableFooterView = UIView()
    }
}

extension PodcastsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath == tableView.indexPathForLastRow {
            showBottomSpinner()
            viewModel.fetchPodcasts()
        }
    }
}
