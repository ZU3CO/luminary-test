//
//  PodcastCellVM.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/28/21.
//

import Foundation

final class PodcastCellVM {
    var title: String
    var publisherName: String
    var thumbnailUrl: URL?
    
    init(podcast: Podcast) {
        title = podcast.title
        publisherName = podcast.publisherName
        thumbnailUrl = podcast.images.thumbnail
    }
}
