//
//  PodcastTableViewCell.swift
//  LuminaryTestApp
//
//  Created by Pavel on 7/27/21.
//

import UIKit

class PodcastTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupSubviews(with viewModel: PodcastCellVM) {
        thumbnailImageView.sd_setImage(with: viewModel.thumbnailUrl, placeholderImage: UIImage(named: "image"))
        
        titleLabel.text = viewModel.title
        publisherLabel.text = viewModel.publisherName
    }
}
